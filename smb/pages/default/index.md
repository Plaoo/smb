## Description

Smb use [markdown sintax.](https://en.wikipedia.org/wiki/Markdown)

[Here](http://marchi.ricerca.di.unimi.it/Teaching/WebComm2017/B10/Markdown%20Cheatsheet%20%C2%B7%20adam-p_markdown-here%20Wiki.html) you can find a great guide.

```python
def greetings():
    """Welcome to Simple Markdown Blog"""
    saluto = "Have Fun!"
    return saluto

```


## Command line options
```
Usage: smb.py [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  config    Return values in config.yaml file and check errors
  new-post  Create a new post HOW TO USE: "./smb.py new_post NAME"
  start     Start sbl server HOW TO USE: "./smb start PORT" default: 8000

```

### or you can use Docker Compose:

```bash
$> docker-compose up --build
# smb ready on 0.0.0.0:8000
```
If you are a CSS expert or want to contribute to the project, you will find the source [here](https://gitlab.com/Plaoo/smb),
you are free to improve it, break it and have fun
