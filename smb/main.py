from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from core_utils import get_config, transform_html, create_posts_list

import logging

#from smb import core_utils

log = logging.getLogger(__name__)

# TODO docs
app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")

config = get_config()

path_pages = config["config"]["path_pages"]
path_posts = config["config"]["path_posts"]
port = config["config"]["port"]

index = config["pages"]["index"]
about = config["pages"]["about"]
header = config["pages"]["header"]
header_path = path_pages + config["pages"]["header"]


@app.get("/", response_class=HTMLResponse)
def homepage():
    t_page = transform_html(path=f"{path_pages}{index}", header=f"{header_path}")

    return HTMLResponse(content=t_page, status_code=200)


@app.get("/about/", response_class=HTMLResponse)
def about_page():
    t_page = transform_html(path=f"{path_pages}{about}", header=f"{header_path}")

    return HTMLResponse(content=t_page, status_code=200)


@app.get("/posts/", response_class=HTMLResponse)
def post_pages():
    t_page = create_posts_list(path_post=path_posts, header=f"{header_path}")

    return HTMLResponse(content=t_page, status_code=200)


@app.get("/posts/{post}", response_class=HTMLResponse)
def posts(post):

    t_page = transform_html(path=f"{path_posts}{post}", header=f"{header_path}")

    return HTMLResponse(content=t_page, status_code=200)


def run(port=8000):
    import uvicorn

    uvicorn.run("main:app", host="0.0.0.0", port=port, reload=True, debug=True)

if __name__ == "__main__":
    run()
