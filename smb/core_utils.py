import yaml
import markdown
import markdown.extensions.fenced_code
import markdown.extensions.codehilite
import os
from urllib.parse import quote
from datetime import datetime
from pathlib import Path
import logging

log = logging.getLogger(__name__)


def get_config():
    with open("config.yaml", "r") as config_file:
        cfg = yaml.safe_load(config_file)
    return cfg


def add_style():
    css = get_config()
    style = False
    if css["config"]["css"]:
        file_css = css["config"]["css"]
        style = f"""
        <!DOCTYPE html>
            <link rel="stylesheet" href="{file_css}"></link>
            """
        return style
    else:
        return ""


def transform_html(path, header=False):
    style = add_style()
    page = merge_page(header=header, page=path)
    t_page = markdown.markdown(f"""{page}""", extensions=["codehilite", "fenced_code"])
    t_page = style + t_page

    return t_page


def create_posts_list(path_post, header=False):
    style = add_style()

    file_list = sorted(Path(path_post).iterdir(), key=os.path.getmtime, reverse=True)
    page = """"""
    for file_l in file_list:
        creation_time = datetime.fromtimestamp(
            os.path.getctime(f"{path_post}{file_l.name}")
        ).strftime("%Y-%m-%d")
        file_n_ex = Path(file_l.name).stem
        file_l = quote(file_l.name)
        page += f"""\r\n- [{creation_time} - {file_n_ex}]({file_l})"""

    print(page)
    page = merge_page(header=header, page=page)
    print(page)

    t_page = markdown.markdown(page, extensions=["codehilite", "fenced_code"])
    t_page = style + t_page
    print(t_page)
    return t_page


def merge_page(page=False, header=False):
    try:
        with open(page, "r") as f:
            page = f.read()
    except Exception:
        pass

    if header:
        with open(header, "r") as f:
            header = f.read()

    if header:
        return header + page
    else:
        return page
