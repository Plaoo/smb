#!/usr/bin/env python3

import main
import click
import os
from core_utils import get_config
import argparse
config_file = get_config()
port = config_file["config"]["port"]
path_posts = config_file["config"]["path_posts"]

def new_post(filename):
    """ Create a new post HOW TO USE: "./smb.py new_post NAME"
    """
    if filename:
        if not path_posts:
            return f"""No directory for posts, you need to configure the config.yml file
                    using ./smb config
                    """
        else:
            new_file = f"{path_posts}{filename}.md"
            open(new_file, "w")
            print(f'Created a new file {path_posts}{filename}.md')
    else:
        print(f"USE: ./smb.py --new_post NAME")



def config():
    """Return values in config.yaml file and check errors"""
    if config:
        try:
            with open('config.yaml', 'r') as f:
                print(f'Config file found! {os.path.realpath(f.name)}')
                print(f.read())
        except Exception:
            print('No config file found, create a new config.yaml')


def command_line():
    parser = argparse.ArgumentParser(description="SMB Simple Markdown Blog command line")
    parser.add_argument("--new_post", help="Create a new post. Example: ./smb.py --new_post Hello", type=str)
    parser.add_argument("--port", help="Start SMB at specific port. Example: ./smb.py --port 80", type=int)
    parser.add_argument("--config", help="Print current config file if exist")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = command_line()
    if args:
        if args.new_post:
            new_post(args.new_post)
        elif args.port:
            main.run(port=args.port)
        elif args.config:
            config()
        elif args.port ==None and args.config==None and args.new_post==None:
            print(f"Start SMB server")
            main.run()


