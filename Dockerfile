# Minimal Dockerfile for smb

FROM python:3.9-slim

WORKDIR /usr/src/smb
ENV PYTHONBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1 


RUN apt-get update \
    && apt-get clean

RUN pip install --upgrade pip

COPY ./requirements.txt .


RUN pip install --no-cache-dir -r requirements.txt

COPY . .
